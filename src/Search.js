import React, { Component } from 'react';
import axios from 'axios';
import Recipe from './Recipe'

const APP_ID = "8d603356"
const APP_KEY = "8f6acc323ca44856aa348746d2fdd7d3"  
    
class Search extends Component {

    constructor(props){
        super(props)
        this.state= {
            search: "",
            searchResultRecipes:[],
            selectedRecipe:{},
            displayRecipe : false            
        }

    }

    getRecipes =()=>{
        console.log("called getRecipes")
        axios.get(`https://api.edamam.com/search?q=${this.state.search ? this.state.search: "chicken" }&app_id=${APP_ID}&app_key=${APP_KEY}`)
        .then(res => {
            const searchResultRecipes = res.data.hits
            this.setState({ searchResultRecipes})
        })

    }
    componentDidMount(){
        this.getRecipes()
    }

    handelOnChange =(e)=>{
        this.setState({
            search : e.target.value
        })
    }

    handelSubmit = (e)=>{
        e.preventDefault()
        this.getRecipes()
        this.setState({ displayRecipe : false    })
    }

  
    handleSelectingRecipe =(e) =>{
        console.log("handleSelectingRecipe called")
        const nameOfSelectedRecipe = e.target.innerHTML
        const selectedRecipe = this.state.searchResultRecipes.find( recipe => recipe.recipe.label === nameOfSelectedRecipe )
        this.setState({ selectedRecipe , displayRecipe : true   })
    }

    
    
    
    render() {
     
        return (
            <div className="Search">
            <form  onSubmit={this.handelSubmit} className="search-form">
                <input  
                className="search-input" 
                type="text"
                value = {this.state.search}
                onChange ={this.handelOnChange}
                />
                <button className="search-button" type="submit">search</button>
            </form>
            <ul>
                {this.state.searchResultRecipes.map((recipe , index)=>(
                    <li key={index} onClick={this.handleSelectingRecipe}  >{recipe.recipe.label}</li>
                ))}
            </ul>
             
            {this.state.displayRecipe  && 
            <Recipe recipe={this.state.selectedRecipe} />       
            }
             

        

          </div>
        );
    }
}

export default Search;