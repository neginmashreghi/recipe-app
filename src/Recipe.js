import React, { Component } from 'react';

class Recipe extends Component {
    render() {
        const recipe = this.props.recipe
        const lable = recipe.recipe.lable
        const calories = recipe.recipe.calories
        const image = recipe.recipe.image
        const ingredients = recipe.recipe.ingredients


        return (
            <div className="recipe">
                <h1>{lable}</h1>
                
                <img src={image} alt={lable} />
                <h3> calories : {Math.round(calories)} cal</h3>
                <ol>
                {ingredients.map((ingredient, index) =>(
                        <li key={index} >{ingredient.text}</li>
                ))}

                </ol>
            
            </div>
        );
    }
}

export default Recipe;